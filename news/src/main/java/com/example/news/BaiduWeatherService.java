package com.example.news;

import com.example.news.bean.BWeatherInfo;
import com.example.news.bean.Current;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class BaiduWeatherService {

    public static final String API_URL = "http://apis.baidu.com/";
    public static final String API_KEY = "c819ef4f9524dda6b8b6ea14eeacc3d1";

    private BaiduWeatherInterface mWeatherAPI;

    public BaiduWeatherService(BaiduWeatherInterface serviceInterface) {
        mWeatherAPI = serviceInterface;
    }

    public static BaiduWeatherService getInstance() {

        // create log interceptor for okhttp3
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);
        httpClient.addNetworkInterceptor(new StethoInterceptor());

        // create gson instance for DateTime deserializer
        Gson gson = new GsonBuilder().registerTypeAdapter(BWeatherInfo.class, new BaiduWeatherInfoDeserializer()).create();

        Retrofit retrofit = new Retrofit.Builder()
                .client(httpClient.build())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(API_URL)
                .build();

        return new BaiduWeatherService(retrofit.create(BaiduWeatherInterface.class));
    }

    public Observable<BWeatherInfo> getCity(String cityName) {
        return mWeatherAPI.getCurrent(API_KEY, cityName)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

}
