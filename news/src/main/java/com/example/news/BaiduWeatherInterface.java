package com.example.news;

import com.example.news.bean.BWeatherInfo;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;
import rx.Observable;


public interface BaiduWeatherInterface {

    @GET("heweather/weather/free")
    Observable<BWeatherInfo> getCurrent(@Header("apikey") String apiKey, @Query("city") String city);
}
