package com.example.news.database;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.Migration;
import com.raizlabs.android.dbflow.annotation.ModelContainer;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.SQLiteType;
import com.raizlabs.android.dbflow.sql.migration.AlterTableMigration;
import com.raizlabs.android.dbflow.structure.BaseModel;

/**
 * Created by Oscar.Chen on 4/13/2016.
 */

@Migration(version = 2, database = ContactDatabase.class)
public class PeopleV2 extends AlterTableMigration<People> {

    public PeopleV2(Class<People> table) {
        super(table);
    }

    @Override
    public void onPreMigrate() {
        addColumn(SQLiteType.TEXT, People_Table.mobile.getNameAlias().getName());
        addColumn(SQLiteType.TEXT, People_Table.phone.getNameAlias().getName());
    }
}