package com.example.news.database;

import com.raizlabs.android.dbflow.annotation.Migration;
import com.raizlabs.android.dbflow.sql.SQLiteType;
import com.raizlabs.android.dbflow.sql.migration.AlterTableMigration;

/**
 * Created by Oscar.Chen on 4/13/2016.
 */

@Migration(version = 3, database = ContactDatabase.class)
public class PeopleV3 extends AlterTableMigration<People> {

    public PeopleV3(Class<People> table) {
        super(table);
    }

    @Override
    public void onPreMigrate() {
        addColumn(SQLiteType.INTEGER, People_Table.enable.getNameAlias().getName());
    }
}