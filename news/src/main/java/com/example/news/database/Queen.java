package com.example.news.database;

import android.graphics.Color;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ModelContainer;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

/**
 * Created by Oscar.Chen on 4/13/2016.
 */

@ModelContainer
@Table(database = ColonyDatabase.class)
public class Queen extends BaseModel {

    @PrimaryKey(autoincrement = true)
    public long id;

    @Column
    public String name;

//    @Column
//    Colony colony;

}
