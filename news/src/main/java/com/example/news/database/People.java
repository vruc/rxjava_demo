package com.example.news.database;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ModelContainer;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

/**
 * Created by Oscar.Chen on 4/13/2016.
 */

@ModelContainer
@Table(database = ContactDatabase.class)
public class People extends BaseModel {

    @PrimaryKey(autoincrement = true)
    public long id;

    @Column
    public String name;

    @Column
    public boolean gender;

    @Column
    public String  email;

    @Column
    public String  phone;

    @Column
    public String  mobile;

    @Column
    public boolean enable;
}
