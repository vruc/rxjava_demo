package com.example.news.bean;

import com.google.gson.annotations.SerializedName;
import lombok.ToString;

/**
 * Created by Oscar.Chen on 3/31/2016.
 */


//    {
//        "coord": {
//        "lon": -0.13,
//        "lat": 51.51
//        },
//        "weather": [
//        {
//        "id": 521,
//        "main": "Rain",
//        "description": "shower rain",
//        "icon": "09d"
//        }
//        ],
//        "base": "cmc stations",
//        "main": {
//        "temp": 289.49,
//        "pressure": 993,
//        "humidity": 67,
//        "temp_min": 285.93,
//        "temp_max": 291.15
//        },
//        "wind": {
//        "speed": 8.7,
//        "deg": 210,
//        "gust": 14.4
//        },
//        "rain": {
//        "1h": 1.02
//        },
//        "clouds": {
//        "all": 40
//        },
//        "dt": 1442242382,
//        "sys": {
//        "type": 1,
//        "id": 5091,
//        "message": 0.0052,
//        "country": "GB",
//        "sunrise": 1442208848,
//        "sunset": 1442254609
//        },
//        "id": 2643743,
//        "name": "London",
//        "cod": 200
//        }
public class Current {
    @SerializedName("id")
    public Integer    mId;
    @SerializedName("name")
    public String     mName;
    @SerializedName("cod")
    public Integer    mCod;
    @SerializedName("coord")
    public Coordinate mCoord;
}
