package com.example.news;

import android.app.Application;

import com.facebook.stetho.Stetho;
import com.raizlabs.android.dbflow.config.FlowManager;

/**
 * Created by Oscar.Chen on 4/13/2016.
 */
public class NewsApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        FlowManager.init(this);
        Stetho.initializeWithDefaults(this);

        PreferencesManager.initializeInstance(this);
        PreferencesManager prefs = PreferencesManager.getInstance();

        prefs.setString("Identifier", "9426A187-9383-4C7A-A8B0-572A8D75AA82");
        prefs.setString("RecoveryKey", "364804-464189-163427-101453-177881-207592-039314-573353");
    }
}
