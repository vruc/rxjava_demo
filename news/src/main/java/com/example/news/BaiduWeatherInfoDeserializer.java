package com.example.news;

import com.example.news.bean.BWeatherInfo;
import com.google.gson.*;

import java.lang.reflect.Type;

/**
 * Created by Oscar.Chen on 4/12/2016.
 */
public class BaiduWeatherInfoDeserializer implements JsonDeserializer<BWeatherInfo> {
    @Override
    public BWeatherInfo deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        JsonObject raw = json.getAsJsonObject();
        String key = "HeWeather data service 3.0";

        JsonObject obj = raw.getAsJsonArray(key).get(0).getAsJsonObject();
        System.out.println("Deserializer " + obj.toString());

        Gson gson = new GsonBuilder().create();
        return gson.fromJson(obj, BWeatherInfo.class);
    }
}