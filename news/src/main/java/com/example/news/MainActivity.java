package com.example.news;

import android.content.Context;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.news.database.Colony;
import com.example.news.database.ContactDatabase;
import com.example.news.database.People;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements OnClickListener {

    @Bind(R.id.mCityName)
    EditText mCityName;
    @Bind(R.id.mQuery)
    Button   mQuery;
    @Bind(R.id.mInfo)
    TextView mInfo;

    @OnClick(R.id.mQuery)
    void query() {
        String city = mCityName.getText().toString();
        Toast.makeText(this, city, Toast.LENGTH_LONG).show();

        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

        People p = new People();
        p.name = city;
        p.email = city + "@info.cn";

        p.mobile = "138" + System.currentTimeMillis();
        p.phone = "180" + System.currentTimeMillis();

        p.gender = System.currentTimeMillis() % 2 == 0;
        p.enable = System.currentTimeMillis() % 2 == 0;

        p.save();

        BaiduWeatherService.getInstance().getCity(city).subscribe(info -> {
            mInfo.setText(info.getStatus());
        }, err -> {
            err.printStackTrace();
        });

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        Colony c = new Colony();
        c.name = ("Colony A_" + System.currentTimeMillis());
        c.save();
    }

    @Override
    public void onClick(View view) {

    }
}
