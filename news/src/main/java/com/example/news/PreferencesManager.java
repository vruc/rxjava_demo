package com.example.news;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferencesManager {

    private static final String PREF_NAME = "CoreData";

    private static PreferencesManager sInstance;
    private final SharedPreferences mPref;

    private PreferencesManager(Context context) {
        mPref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public static synchronized void initializeInstance(Context context) {
        if (sInstance == null) {
            sInstance = new PreferencesManager(context);
        }
    }

    public static synchronized PreferencesManager getInstance() {
        if (sInstance == null) {
            throw new IllegalStateException(PreferencesManager.class.getSimpleName() +
                    " is not initialized, call initializeInstance(..) method first.");
        }
        return sInstance;
    }

    public void setString(String key, String value){
        mPref.edit()
                .putString(key, value)
                .commit();
    }

    public void setInt(String key, int value){
        mPref.edit()
                .putInt(key, value)
                .commit();
    }

    public void setBoolean(String key, Boolean value){
        mPref.edit()
                .putBoolean(key, value)
                .commit();
    }

    public String getString(String key){
        return mPref.getString(key, "");
    }

    public int getInt(String key){
        return mPref.getInt(key, 0);
    }

    public boolean getBoolean(String key){
        return mPref.getBoolean(key, false);
    }

    public void remove(String key) {
        mPref.edit()
                .remove(key)
                .commit();
    }

    public boolean contains(String key){
        return mPref.contains(key);
    }

    public boolean clear() {
        return mPref.edit()
                .clear()
                .commit();
    }
}

