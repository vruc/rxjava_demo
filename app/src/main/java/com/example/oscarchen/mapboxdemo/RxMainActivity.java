package com.example.oscarchen.mapboxdemo;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.oscarchen.mapboxdemo.model.GithubServiceManager;
import com.example.oscarchen.mapboxdemo.model.GithubUser;
import com.example.oscarchen.mapboxdemo.model.GithubUserDetail;
import com.example.oscarchen.mapboxdemo.util.UiHelper;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Observable;


public class RxMainActivity extends AppCompatActivity implements OnClickListener {

    public interface MyFunction {
        @Nullable
        String run();
    }

    @Bind(R.id.mLambda)
    Button   mLambda;
    @Bind(R.id.mGetUsers)
    Button   mGetUsers;
    @Bind(R.id.mGetUserDetails)
    Button   mGetUserDetails;
    @Bind(R.id.mEditText)
    TextView mEditText;
    @Bind(R.id.mUserList)
    ListView mUserList;

    private ArrayAdapter<String> _adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rx_main);

        ButterKnife.bind(this);

        mLambda.setOnClickListener(RxMainActivity.this);
        mGetUsers.setOnClickListener(RxMainActivity.this);
        mGetUserDetails.setOnClickListener(RxMainActivity.this);

        _adapter = new ArrayAdapter<>(RxMainActivity.this,
                android.R.layout.activity_list_item,
                android.R.id.text1,
                new ArrayList<>());

        _adapter.add("Hello World");

        mUserList.setAdapter(_adapter);

//        mUserList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//
//            }
//        });

        mUserList.setOnItemClickListener((parent, view, position, id) -> {
            TextView text1 = (TextView) view.findViewById(android.R.id.text1);
            mEditText.setText( text1.getText().toString().split(",")[1] );
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mLambda:
                testLambda();
                break;
            case R.id.mGetUsers:
                getGithubUsers();
                break;
            case R.id.mGetUserDetails:
                getGithubUserDetail();
                break;
        }
    }

    private void getGithubUserDetail() {

        clearList();

        new GithubServiceManager().rxFetchUserDetails(10)
                .flatMap(users -> Observable.from(users))
                .subscribe(user -> {
                    String data = String.format("%s, %s, %s", user.mId, user.mName, user.mLogin);
                    Log.d("TAG_GROUP", data);
                    _adapter.add(data);
                    _adapter.notifyDataSetChanged();
                });

//        new GithubServiceManager().rxFetchUserDetails(10).subscribe(githubUserDetails -> {
//            Log.d("TAG_GROUP", githubUserDetails.size() + "");
//
//            List<String> list = new ArrayList<>();
//            for(GithubUserDetail user : githubUserDetails){
//                list.add(String.format("%s, %s, %s", user.mId, user.mName, user.mLogin));
//            }
//            _adapter.clear();
//            _adapter.addAll(list);
//            _adapter.notifyDataSetChanged();
//        });

    }

    void clearList(){
        _adapter.clear();
        _adapter.notifyDataSetChanged();
    }

    void getGithubUsers() {
        clearList();

        new GithubServiceManager().rxFetchUsers(10).subscribe(githubUsers -> {
            Log.d("TAG_GROUP", githubUsers.size() + "");

            List<String> list = new ArrayList<>();
            for(GithubUser user : githubUsers){
                list.add(String.format("%s, %s", user.mId, user.mLogin));
            }
            _adapter.clear();
            _adapter.addAll(list);
            _adapter.notifyDataSetChanged();
        });
    }

    void testLambda() {

        MyFunction myFunction = () -> "Hello RetroLambda";

//        MyFunction myFunction2 = new MyFunction() {
//            @Nullable
//            @Override
//            public String run() {
//                return "Hello Java 7";
//            }
//        };

        UiHelper.Toast(RxMainActivity.this, myFunction.run());

//        new GithubServiceManager().rxFetchUserDetails("vruc")
//                .subscribe(githubUserDetails -> {
//                    Log.d("TAG_GROUP", githubUserDetails.mId + "");
//                    mEditText.setText(githubUserDetails.toString());
//                });
    }
}