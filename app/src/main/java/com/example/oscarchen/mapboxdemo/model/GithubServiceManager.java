package com.example.oscarchen.mapboxdemo.model;

import android.util.Log;

import java.util.List;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.schedulers.Schedulers;
/**
 * Created by Oscar.Chen on 3/30/2016.
 */
public class GithubServiceManager {

    private GithubService mService;

    public GithubServiceManager() {
        mService = GithubService.getInstance();
    }

    public Observable<List<GithubUserDetail>> rxFetchUserDetails(Integer perPage) {
        //request the users
        return mService.rxRequestUsers(perPage).concatMap(Observable::from)
                .concatMap((GithubUser githubUser) ->
                        {
                            Log.d("TAG_GROUP", githubUser.mLogin);
                            //request the details for each user
                            return mService.rxRequestUserDetails(githubUser.mLogin);
                        }
                )
                //accumulate them as a list
                .toList()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<List<GithubUser>> rxFetchUsers(Integer perPage) {
        //request the users
        return mService.rxRequestUsers(perPage).concatMap(Observable::from)
                .toList()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<List<GithubUserDetail>> rxFetchUserDetails() {
        return rxFetchUserDetails(1);
    }

    public Observable<GithubUserDetail> rxFetchUserDetails(String login){
        Log.d("TAG_GROUP", login);
        return mService.rxRequestUserDetails(login)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

}
