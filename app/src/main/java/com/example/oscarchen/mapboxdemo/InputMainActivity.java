package com.example.oscarchen.mapboxdemo;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.example.oscarchen.mapboxdemo.model.GithubServiceManager;
import com.example.oscarchen.mapboxdemo.util.RelativeDateFormat;
import com.example.oscarchen.mapboxdemo.util.UiHelper;
import com.mapbox.mapboxsdk.annotations.Annotation;
import com.mapbox.mapboxsdk.annotations.Icon;
import com.mapbox.mapboxsdk.annotations.IconFactory;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.geometry.LatLngBounds;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Duration;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import butterknife.Bind;
import butterknife.ButterKnife;


public class InputMainActivity extends AppCompatActivity implements OnClickListener {

    @Bind(R.id.mUserName)
    EditText mUserName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_main);

        ButterKnife.bind(this);

        mUserName.setFilters(new InputFilter[]{new FirstCapFilter()});
    }

    @Override
    public void onClick(View view) {

    }

    class FirstCapFilter implements InputFilter {

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
////            Log.d("AAA", "source:[" + source.toString() + "], dest:[" + dest.toString() + "]");
////            Log.d("AAA", "start->" + start + ",end->" + end + ",dstart->" + dstart + ",dend->" + dend);
//
//            Log.d("AAA", "source:[" + source.toString() + "]");
//            Log.d("AAA", "start->" + start + ",end->" + end);
//
////            if (source.length() == 1) {
////                return source.toString().toUpperCase();
////            }
//
//            return source.toString().toUpperCase();
//
////            return null;

            Log.d("AAA", String.format("%d %d %d %d", start, end, dstart, dend));
            Log.d("AAA", String.format("[%s] [%s]", source, dest));

//            if(end - start == 1 && dstart ==0 && dend ==0){
//                return source.toString().toUpperCase();
//            }

//            for (int i = start; i < end; i++) {
//                if (Character.isLowerCase(source.charAt(i))) {
//                    char[] v = new char[end - start];
//                    TextUtils.getChars(source, start, end, v, 0);
//                    String s = new String(v).toUpperCase();
//
//                    if (source instanceof Spanned) {
//                        SpannableString sp = new SpannableString(s);
//                        TextUtils.copySpansFrom((Spanned) source, start, end, null, sp, 0);
//                        Log.d("AAA", String.format("[%s]", sp.toString()));
//                        return sp;
//                    } else {
//                        Log.d("AAA", String.format("[%s]", s));
//                        return s;
//                    }
//                }
//            }

            int mMaxLenth = 20;
            int sourceLen = source.toString().length();
            int destLen   = dest.toString().length();
            if (sourceLen + destLen > mMaxLenth) {
                return "";
            }
            return null;

        }
    }
}