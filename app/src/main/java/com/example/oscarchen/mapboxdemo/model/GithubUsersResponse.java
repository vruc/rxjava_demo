package com.example.oscarchen.mapboxdemo.model;

/**
 * Created by Oscar.Chen on 3/30/2016.
 */
import java.util.List;

import lombok.ToString;

@ToString
public class GithubUsersResponse {
    List<GithubUser> mGithubUsers;
}
