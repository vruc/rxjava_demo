package com.example.oscarchen.mapboxdemo.util;

/**
 * Created by medici on 8/26/14.
 */

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Duration;
import org.joda.time.format.DateTimeFormatter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class RelativeDateFormat {

    private static final long MINUTE = 60000L;
    private static final long HOUR   = 60L * MINUTE;
    private static final long DAY    = 24L * HOUR;
    private static final long WEEK   = 7L *  DAY;
    private static final long MONTH  = 30L * DAY;

    public static ArrayList<String> format(Date date, DateTimeFormat type) {
        ArrayList<String> result = new ArrayList<String>();

        DateTime utcNow = new DateTime(DateTimeZone.UTC);
        DateTime dateTime = new DateTime(date);

        Duration duration = getTimeDiff(dateTime, utcNow);

        long delta = duration.getMillis();

        String subTitle = "";

//      NOT_YET
        if(delta < 0 || date.getTime() < 0) {
            result.add("");
            result.add("TimeNotYet");
            result.add("");
            return result;
        }

//      ONE_SECOND_AGO
        if( delta == 1L){
            result.add("1");
            result.add("Time_OneSecondAgo");
            result.add(subTitle);
            return result;
        }

//      SECONDS_AGO
        if( delta < MINUTE){
            result.add( toSeconds(delta)+"" );
            result.add("Time_SecondsAgo");
            result.add(subTitle);
            return result;
        }

//      ONE_MINUTE_AGO
        if( delta < 2L * MINUTE){
            result.add("1");
            result.add("Time_OneMinuteAgo");
            result.add(subTitle);
            return result;
        }

//      MINUTES_AGO
        if( delta < 60L * MINUTE){
            result.add(toMinutes(delta)+"");
            result.add("Time_MinutesAgo");
            result.add(subTitle);
            return result;
        }

//      ONE_HOUR_AGO
        if( delta < 90L * MINUTE){
            result.add("1");
            result.add("Time_OneHourAgo");
            result.add(subTitle);
            return result;
        }

//      HOURS_AGO
        if( delta < 24L * HOUR){
            result.add(toHours(delta)+"");
            result.add("Time_HoursAgo");
            result.add(subTitle);
            return result;
        }

//      YESTERDAY
        if( delta < 48L * HOUR){
            if(type == DateTimeFormat.Full){
                result.add("1");
                result.add("Time_OneDayAgo");
                result.add(subTitle);
            } else if( type == DateTimeFormat.Less){
                result.add("");
                result.add("Time_Yesterday");
                result.add("");
            }

            return result;
        }

//      DAYS_AGO
        if( delta < 1L * MONTH){
            result.add(toDays(delta)+"");
            result.add("Time_DaysAgo");
            result.add(subTitle);
            return result;
        }

//      ONE_MONTH_AGO
        if( delta < 2L * MONTH){
            result.add("1");
            result.add("Time_OneMonthAgo");
            result.add(subTitle);
            return result;
        }

//      MONTHS_AGO
        if( delta < 12L * MONTH){
            result.add(toMonths(delta)+"");
            result.add("Time_MonthsAgo");
            result.add(subTitle);
            return result;
        } else {
            long years = toYears(delta);
            if(years <= 1){
                result.add("1");
                result.add("Time_OneYearAgo");
                result.add(subTitle);
                return result;
            } else {
                result.add(years+"");
                result.add("Time_YearsAgo");
                result.add(subTitle);
                return result;
            }
        }
    }

    public static ArrayList<String> format(Date date) {
        return format(date, DateTimeFormat.Less);
    }

    public static String formatToString(Date date){
        ArrayList<String> formatResult = format(date, DateTimeFormat.Less);
        return (formatResult.get(2) + " " + formatResult.get(0) + " " + formatResult.get(1)).trim();
    }

    public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
        long diffInMillies = date2.getTime() - date1.getTime();
        return timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);
    }

    private static long toSeconds(long date) {
        return date / 1000L;
    }

    private static long toMinutes(long date) {
        return toSeconds(date) / 60L;
    }

    private static long toHours(long date) {
        return toMinutes(date) / 60L;
    }

    private static long toDays(long date) {
        return toHours(date) / 24L;
    }

    private static long toMonths(long date) {
        return toDays(date) / 30L;
    }

    private static long toYears(long date) {
        return toMonths(date) / 12L;
    }

    public static Date parseUTCTime(String time){
        SimpleDateFormat isoFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        isoFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        try{
            Date date = isoFormat.parse(time);
            return date;
        } catch(ParseException ex){
            return null;
        }
    }

    private static Date getUTCDate(){
        SimpleDateFormat dateFormatGmt = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss");
        dateFormatGmt.setTimeZone(TimeZone.getTimeZone("GMT"));
        SimpleDateFormat dateFormatLocal = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss");
        try{
            return dateFormatLocal.parse( dateFormatGmt.format(new Date()) );
        } catch(ParseException ex){
            return null;
        }
    }

    public static DateTime convertStringToUTCTime(String time){
        DateTime dUtc = new DateTime(time, DateTimeZone.UTC);
        return dUtc;
    }

    public static DateTime convertStringToLocalTime(Object time){
        TimeZone local = TimeZone.getDefault();
        DateTime dLocal = new DateTime(time, DateTimeZone.forID(local.getID()));
        return dLocal;
    }

    public static String formatDateTime(DateTime time, DateTimeFormatter fmt){
        return fmt.print(time);
    }

    public static String formatDateTime(DateTime time){
        DateTimeFormatter fmt = org.joda.time.format.DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
        return formatDateTime(time, fmt);
    }

    public static Duration getTimeDiff(DateTime t1, DateTime t2){
        Duration d = new Duration(t1, t2);
        return d;
    }


}
