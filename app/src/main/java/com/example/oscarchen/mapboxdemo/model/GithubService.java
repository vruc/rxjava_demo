package com.example.oscarchen.mapboxdemo.model;

/**
 * Created by Oscar.Chen on 3/30/2016.
 */

import java.util.List;

import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Path;
import rx.Observable;

public class GithubService {

    public static final int PER_PAGE = 1;
    public static String ENDPOINT = "https://api.github.com";

    private GithubAPIInterface mGithubAPI;

    public GithubService(GithubAPIInterface githubAPI) {
        mGithubAPI = githubAPI;
    }

    public static GithubService getInstance() {
        Retrofit restAdapter = new Retrofit.Builder()
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(ENDPOINT)
                .build();

        return new GithubService(restAdapter.create(GithubAPIInterface.class));
    }

    public Observable<GithubUserDetail> rxRequestUserDetails(@Path("username") String username) {
        return mGithubAPI.rxRequestUserDetails(username);
    }

    public Observable<List<GithubUser>> rxRequestUsers() {
        return rxRequestUsers(PER_PAGE);
    }

    public Observable<List<GithubUser>> rxRequestUsers(Integer perPage) {
        return mGithubAPI.rxRequestUsers(perPage);
    }

}
