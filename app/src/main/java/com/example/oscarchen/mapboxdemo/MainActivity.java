package com.example.oscarchen.mapboxdemo;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.example.oscarchen.mapboxdemo.model.GithubServiceManager;
import com.example.oscarchen.mapboxdemo.util.RelativeDateFormat;
import com.example.oscarchen.mapboxdemo.util.UiHelper;
import com.mapbox.mapboxsdk.annotations.Annotation;
import com.mapbox.mapboxsdk.annotations.Icon;
import com.mapbox.mapboxsdk.annotations.IconFactory;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.constants.Style;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.geometry.LatLngBounds;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Duration;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import butterknife.Bind;
import butterknife.ButterKnife;


public class MainActivity extends AppCompatActivity implements OnClickListener {

    public interface MyFunction {
        @Nullable
        String run();
    }

    private MapboxMap mMapboxMap;

    @Bind(R.id.mMapView)
    MapView mMapView;

    @Bind(R.id.mFitBounds)
    Button mFitBounds;
    @Bind(R.id.mChangeMapStyle)
    Button mChangeMapStyle;
    @Bind(R.id.mClearMarker)
    Button mClearMarker;
    @Bind(R.id.mClearMarker2)
    Button mClearMarker2;
    @Bind(R.id.mAddMarkers)
    Button mAddMarkers;

//    @Bind(R.id.mLambda)
//    Button   mLambda;

//    @Bind(R.id.mShowTimeDiff)
//    Button   mShowTimeDiff;
//    @Bind(R.id.mGetUser)
//    Button   mGetUser;
//    @Bind(R.id.mEditText)
//    TextView mEditText;


    private int idx = 0;

    LatLng gz  = new LatLng(23.1231073, 113.3222251);
    LatLng sz  = new LatLng(23.1231073, 115.3222251);
    LatLng jd1 = new LatLng(25.1231073, 111.3222251);
    LatLng jd2 = new LatLng(21.1231073, 111.3222251);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        mLambda = (Button) findViewById(R.id.mLambda);
//        mGetUser = (Button) findViewById(R.id.mGetUser);
//        mEditText = (TextView) findViewById(R.id.mEditText);
//


        ButterKnife.bind(this);

        mMapView.onCreate(savedInstanceState);
        mMapView.findViewById(R.id.attributionView);

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(@NonNull final MapboxMap mapboxMap) {

                Log.d("TAG", "onMapReady");
                mMapboxMap = mapboxMap;

//                Marker marker1 = mMapboxMap.addMarker(new MarkerOptions().position(gz));
//                Marker marker2 = mMapboxMap.addMarker(new MarkerOptions().position(sz));
//                Marker marker3 = mMapboxMap.addMarker(new MarkerOptions().position(jd1));
//                Marker marker4 = mMapboxMap.addMarker(new MarkerOptions().position(jd2));
//
//
//                mMarkers.add(marker1);
//                mMarkers.add(marker2);
//                mMarkers.add(marker3);
//                mMarkers.add(marker4);
//
//                CameraPosition cameraPosition = new CameraPosition.Builder()
//                        .target(gz)
//                        .zoom(1)
//                        .tilt(30)
//                        .build();
//
//                mMapboxMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
//
//                addMarkers();

                mMapboxMap.setStyle(Style.SATELLITE);

                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(new LatLng(lat, lng))
                        .zoom(4)
                        .tilt(30)
                        .build();

                mMapboxMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                mFitBounds.setOnClickListener(MainActivity.this);
                mChangeMapStyle.setOnClickListener(MainActivity.this);
                mClearMarker.setOnClickListener(MainActivity.this);
                mClearMarker2.setOnClickListener(MainActivity.this);
                mAddMarkers.setOnClickListener(MainActivity.this);


                mMapboxMap.setOnMarkerClickListener(marker -> {
                    Log.d("AAA", marker.getSnippet());

                    int i = 0;

                    List<Annotation> annotations = mMapboxMap.getAnnotations();
                    for (Annotation annotation : annotations) {
                        i++;
                        if (annotation instanceof Marker) {
                            Marker m = (Marker) annotation;
                            if (m.getSnippet() == marker.getSnippet()) {
                                Log.d("AAA", i + "");
                            }
                        }
                    }

                    return false;
                });

//                mLambda.setOnClickListener(MainActivity.this);
//                mShowTimeDiff.setOnClickListener(MainActivity.this);
//                mLambda.setOnClickListener(MainActivity.this);
//                mGetUser.setOnClickListener(MainActivity.this);
            }
        });
    }

    private List<Marker> mMarkers = new ArrayList<>();

//    private double lat  = 23.1d;
//    private double lng  = 113.1d;

    private double lat  = 47.3816022d;
    private double lng  = 8.5350387d;
    private double step = 0.0005d;

    void addMarkers() {
        Log.d("addMarkers", "========================================================================================================");
        Random rnd = new Random(System.currentTimeMillis());
        for (int i = 0; i < 1; i++) {

            LatLng latlng = new LatLng(lat + step, lng + step);
            Log.d("addMarkers", latlng.toString());

            Icon icon = IconFactory.getInstance(MainActivity.this).fromResource(R.mipmap.ic_launcher);
            Marker marker = mMapboxMap.addMarker(new MarkerOptions()
//                    .icon(icon)
                    .snippet(step + "")
                    .position
                            (latlng));
            mMarkers.add(marker);

            lat = lat + step;
            lng = lng + step;
        }
        Log.d("addMarkers", "========================================================================================================");
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mMapView.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mChangeMapStyle:
                toggleMapStyle();
                break;
            case R.id.mFitBounds:
                fitBounds();
                break;
//            case R.id.mShowTimeDiff:
//                testJoda();
//                break;
            case R.id.mLambda:
                testLambda();
                break;
//            case R.id.mGetUser:
//                getGithubUser();
//                break;
            case R.id.mClearMarker:
                clearMarker();
                break;
            case R.id.mClearMarker2:
                clearMarker2();
                break;
            case R.id.mAddMarkers:
                addMarkers();
                break;
        }
    }

    void clearMarker2() {
        Log.d("clearMarker2", "clear list mMarkers -> " + mMarkers.size());
        for (Marker marker : mMarkers) {
            Log.d("clearMarker2", marker.getId() + "");
            mMapboxMap.removeMarker(marker);
        }
        mMarkers.clear();
        Log.d("clearMarker2", "clear list mMarkers -> " + mMarkers.size());
    }

    void clearMarker() {
        Log.d("clearMarker before", mMapboxMap.getAnnotations().size() + "");
        for (Annotation ann : mMapboxMap.getAnnotations()) {
            Log.d("AAA", ann.getId() + "");
            mMapboxMap.removeAnnotation(ann);
        }
        mMapboxMap.removeAnnotations();
        Log.d("clearMarker finish", mMapboxMap.getAnnotations().size() + "");
//        mMapboxMap.removeAnnotations();
    }

    void getGithubUser() {
        new GithubServiceManager().rxFetchUserDetails("vruc")
                .subscribe(githubUserDetails -> {
                    Log.d("TAG_GROUP", githubUserDetails.mId + "");
//                    mEditText.setText(githubUserDetails.toString());
                });
    }

    void testLambda() {
        MyFunction myFunction = () -> "Hello RetroLambda";

        MyFunction myFunction2 = new MyFunction() {
            @Nullable
            @Override
            public String run() {
                return "Hello Java 7";
            }
        };

        UiHelper.Toast(MainActivity.this, myFunction.run());

        new GithubServiceManager().rxFetchUserDetails().subscribe(githubUserDetails -> {
            Log.d("TAG_GROUP", githubUserDetails.size() + "");
//            mEditText.setText(githubUserDetails.toString());
        });

    }

    void testJoda() {

        Calendar cal = Calendar.getInstance();
        cal.set(2016, 2, 25, 14, 0, 0);

        DateTime utcNow   = new DateTime(DateTimeZone.UTC);
        DateTime dateTime = new DateTime(cal.getTime());

        Duration d = new Duration(utcNow, dateTime);

        Log.d("TAG_GROUP", d.getMillis() + "");

        ArrayList<String> ret        = RelativeDateFormat.format(cal.getTime());
        String            listString = "";

        for (String s : ret) {
            listString += s + "\t";
        }

        UiHelper.Toast(this, listString);

    }

    void toggleMapStyle() {
        Log.d("AAA", "clear list mMarkers -> " + mMarkers.size());
        mMarkers.clear();
        Log.d("AAA", "clear list mMarkers -> " + mMarkers.size());
//        switch (idx++ % 6) {
//            case 0:
//                mMapboxMap.setStyle(Style.SATELLITE);
//                break;
//            case 1:
//                mMapboxMap.setStyle(Style.DARK);
//                break;
//            case 2:
//                mMapboxMap.setStyle(Style.EMERALD);
//                break;
//            case 3:
//                mMapboxMap.setStyle(Style.LIGHT);
//                break;
//            case 4:
//                mMapboxMap.setStyle(Style.MAPBOX_STREETS);
//                break;
//            case 5:
//                mMapboxMap.setStyle(Style.SATELLITE_STREETS);
//                break;
//        }
//        Log.d("TAG", "toggleMapStyle " + mMapboxMap.getStyleUrl());
    }

    void fitBounds() {

        LatLngBounds bounds = new LatLngBounds.Builder()
                .include(gz)
                .include(sz)
                .include(jd1)
                .include(jd2)
                .build();

        int padding = (int) getResources().getDimension(R.dimen.coordinatebounds_margin);
//
//        switch (idx++ % 3) {
//            case 0:
//                padding = (int) getResources().getDimension(R.dimen.coordinatebounds_margin_small);
//                break;
//            case 1:
//                padding = (int) getResources().getDimension(R.dimen.coordinatebounds_margin);
//                break;
//            case 2:
//            default:
//                padding = (int) getResources().getDimension(R.dimen.coordinatebounds_margin_large);
//                break;
//        }

        Log.d("TAG", "fitBounds " + padding);
        UiHelper.Toast(this, "fitBounds " + padding);
        mMapboxMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, padding));
    }

}