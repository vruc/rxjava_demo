package com.example.oscarchen.mapboxdemo.util;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by Oscar.Chen on 3/29/2016.
 */

public final class UiHelper {

    private static Toast showingToast;

    public static void Toast(Context context, String message, int time) {
        if(showingToast == null) {
            showingToast = Toast.makeText(context, message, time);
        } else {
            showingToast.setText(message);
        }
        showingToast.show();
    }

    public static void Toast(Context context, String message) {
        if(showingToast == null) {
            showingToast = Toast.makeText(context, message, Toast.LENGTH_LONG);
        } else {
            showingToast.setText(message);
        }
        showingToast.show();
    }
}
