package com.example.oscarchen.mapboxdemo.model;

import java.util.List;

import retrofit2.Callback;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Oscar.Chen on 3/30/2016.
 */
interface GithubAPIInterface {

    @GET("/users/{username}")
    Observable<GithubUserDetail> rxRequestUserDetails(@Path("username") String username);

    @GET("/users")
    Observable<List<GithubUser>> rxRequestUsers(@Query("per_page") Integer perPage);

}
